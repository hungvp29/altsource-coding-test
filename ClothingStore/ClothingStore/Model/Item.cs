﻿using System.Collections.Generic;

namespace ClothingStore
{
    public class BasicItem
    {
        public int Id { get; set; }
        public int Category { get; set; }
        public int SubCategory { get; set; }
        public decimal Buy_Price { get; set; }
        public decimal Sell_Price { get; set; }
    }

    public class StoreItem : BasicItem
    {

        public List<int> Color { get; set; }
        public List<int> Size { get; set; }
    }

    public class Procces_Item : BasicItem
    {
        public Procces_Item()
        {
            Quantity = 1;
        }
        public int Quantity { get; set; }
        public int Color { get; set; }
        public int Size { get; set; }
    }
}
