﻿using System;

namespace ClothingStore.Model
{
    public class Transaction
    {
        public DateTime Date { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public int Action { get; set; }
        public string ExtraInfo { get; set; }
    
    }
}
