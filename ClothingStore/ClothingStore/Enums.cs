﻿namespace ClothingStore
{

    public enum ActionType
    {
        None = 0,
        Buy = 1,
        Sell = 2,
        PrintOrders= 3,
        Quit = 4
    }

    public enum Category
    {
        Shirt = 1
    }

    public enum SubCategory
    {
        T_Shirt = 1,
        Dress_Shirt = 2
    }

    public enum Color
    {
        Black = 1,
        White = 2,
        Red = 3
    }

    public enum Size
    {
        Small = 1,
        Medium = 2,
        Large =3
    }


}
