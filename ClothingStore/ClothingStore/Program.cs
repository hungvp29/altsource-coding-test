﻿using ClothingStore.Model;
using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClothingStore
{
    internal class Program
    {
        private static List<StoreItem> listItem = new List<StoreItem>();
        private static readonly List<Transaction> trans = new List<Transaction>();
        private static void Main(string[] args)
        {
            InitMetaData();
            // Display title as the C# console calculator app
            Console.WriteLine("Console ClothingStore in C#\r");
            Console.WriteLine("To select option please input number\r");
            Console.WriteLine("------------------------\n");

            ActionType action = ActionType.None;

            while (action != ActionType.Quit)
            {
                try
                {
                    action = InputAction();
                    switch (action)
                    {
                        case ActionType.Buy:
                        case ActionType.Sell:
                            Procces_Item item = GetItemInfoNeedToProcess();
                            ProcessItem(item, action);
                            break;
                        case ActionType.PrintOrders:
                            PrintAllOrder();
                            break;
                        case ActionType.Quit:
                            Quit();
                            break;
                        default:
                            Console.WriteLine("Invalid input. Please try again!");
                            break;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("There is error when process please try agian");
                }
            }
        }

        private static void InitMetaData()
        {
            listItem.Add(new StoreItem() { Id = 1, Category = (int)Category.Shirt, SubCategory = (int)SubCategory.Dress_Shirt, Color = new List<int> { (int)Color.Black, (int)Color.White }, Size = new List<int> { (int)Size.Small, (int)Size.Large }, Buy_Price = 8, Sell_Price = 20 });
            listItem.Add(new StoreItem() { Id = 2, Category = (int)Category.Shirt, SubCategory = (int)SubCategory.T_Shirt, Color = new List<int> { (int)Color.Black, (int)Color.Red }, Size = new List<int> { (int)Size.Small, (int)Size.Medium }, Buy_Price = 6, Sell_Price = 12 });
        }

        private static void ProcessItem(Procces_Item item, ActionType action)
        {
            //Can be valid if item out of stock or not here

            //Insert transaction
            Transaction transaction = new Transaction()
            {
                Action = (int)action,
                Date = DateTime.Now,
                ExtraInfo = string.Format("Color: {0}, Size: {1}", (Color)item.Color, (Size)item.Size),
                ItemId = item.Id,
                Price = action == ActionType.Buy ? item.Buy_Price : item.Sell_Price,
                Quantity = item.Quantity

            };
            transaction.TotalAmount = transaction.Quantity * transaction.Price;
            trans.Add(transaction);
        }

        private static void Quit()
        {
            // Wait for the user to respond before closing
            Console.Write("Press any key to close the ClothingStore console app...");
            Console.ReadKey();
        }

        private static ActionType InputAction()
        {
            // Ask the user what to do
            Console.WriteLine("What you need to do?\r");
            foreach (ActionType actionType in (ActionType[])Enum.GetValues(typeof(ActionType)))
            {
                if (actionType != ActionType.None)
                {
                    Console.WriteLine(string.Format("{0} - {1}", (int)actionType, actionType.ToString()));
                }
            }

            int selectedAction = (int)ActionType.None;
            if (int.TryParse(Console.ReadLine(), out selectedAction))
            {
                //nothing to do
            }
            return (ActionType)selectedAction;
        }

        private static Procces_Item GetItemInfoNeedToProcess()
        {
            int value = -1;
            Procces_Item item = new Procces_Item();

            //Category
            #region Category
            List<int> lstCategories = listItem.Select(x => x.Category).Distinct().ToList();
            if (lstCategories.Count == 1)
            {
                item.Category = lstCategories.FirstOrDefault();
            }
            else
            {
                while (value == -1)
                {
                    PrintOption(lstCategories, "category");
                    value = GetValidValue(lstCategories);
                    if (value == Containts.CANCEL)
                    {
                        return null;
                    }
                    item.SubCategory = value;
                }
            }
            #endregion

            #region SubCategory
            value = -1;
            List<int> lstSubCategories = listItem.Where(x => x.Category == item.Category).Select(x => x.SubCategory).Distinct().OrderBy(x => x).ToList();
            if (lstSubCategories.Count == 0)
            {
                Console.WriteLine(Containts.OUTOFSTOCK);
                return null;
            }
            if (lstSubCategories.Count == 1)
            {
                item.SubCategory = lstSubCategories.FirstOrDefault();
            }
            else
            {

                while (value == -1)
                {

                    PrintOption(lstSubCategories, "subcategory");
                    value = GetValidValue(lstSubCategories);
                    if (value == Containts.CANCEL)
                    {
                        return null;
                    }
                    item.SubCategory = value;
                }
            }
            #endregion

            StoreItem selected_item = listItem.Where(x => x.Category == item.Category && x.SubCategory == item.SubCategory).FirstOrDefault();
            if (selected_item == null)
            {
                Console.WriteLine(Containts.OUTOFSTOCK);
                return null;
            }
            else
            {
                item.Id = selected_item.Id;
                item.Sell_Price = selected_item.Sell_Price;
                item.Buy_Price = selected_item.Buy_Price;
            }

            #region Color
            value = -1;
            List<int> listColor = listItem.Where(x => x.Id == item.Id).Select(x => x.Color).FirstOrDefault();
            if (listColor.Count == 1)
            {
                item.Color = listColor.FirstOrDefault();
            }
            else
            {

                while (value == -1)
                {
                    PrintOption(listColor, "color");
                    value = GetValidValue(listColor);
                    if (value == Containts.CANCEL)
                    {
                        return null;
                    }
                    item.Color = value;
                }
            }
            #endregion

            #region Size
            value = -1;
            List<int> listSize = listItem.Where(x => x.Id == item.Id).Select(x => x.Size).FirstOrDefault();
            if (listSize.Count == 1)
            {
                item.Size = listSize.FirstOrDefault();
            }
            else
            {

                while (value == -1)
                {
                    PrintOption(listSize, "size");
                    value = GetValidValue(listSize);
                    if (value == Containts.CANCEL)
                    {
                        return null;
                    }
                    item.Size = value;
                }
            }
            #endregion

            #region Quantity
            //TBD
            #endregion

            return item;
        }

        private static void PrintOption(List<int> list, string type)
        {
            Console.WriteLine(String.Format("Please input {0}?\r", type));
            foreach (int item in list)
            {
                switch (type)
                {
                    case "category":
                        Console.WriteLine(string.Format("{0} - {1}", item, (Category)item));
                        break;
                    case "subcategory":
                        Console.WriteLine(string.Format("{0} - {1}", item, (SubCategory)item));
                        break;
                    case "color":
                        Console.WriteLine(string.Format("{0} - {1}", item, (Color)item));
                        break;
                    case "size":
                        Console.WriteLine(string.Format("{0} - {1}", item, (Size)item));
                        break;
                }

               
            }
            Console.WriteLine(string.Format("{0} - {1}", Containts.CANCEL, "Cancel"));
        }

        private static int GetValidValue(List<int> listValidValue)
        {
            int value = -1;
            if (!int.TryParse(Console.ReadLine(), out value))
            {
                return -1;
            }
            if (value != Containts.CANCEL)
            {
                if (!listValidValue.Contains(value))
                {
                    return -1;
                }
            }

            return value;
        }

        private static void PrintAllOrder()
        {
            ConsoleTable.From<Transaction>(trans).Write();
        }
    }
}
